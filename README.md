# qa-assignment

## Synopsis

The project contains the e2e tests that verify a calories calculator application

- Application can be accessed at: http://calories-calc.herokuapp.com/

### Test Execution

A gradle task exists for that matter and can be called via one of the following commands
```
./gradlew ft
./gradlew functionalTest
```

Important Note
- Firefox browser is a required to have been previously installed

The plugin webdrivermanager by bonigarcia was used in this project to take care the download and setup of Firefox web
driver
