package qa.assignment.modules

import geb.Module

class CaloriesTableBody extends Module {

    static content = {
        tableBodyCell { $('td', it) }
        emptyMessage(required: false, wait: 5) { $(tableBodyCell(0)) }
        name(required: false, wait: 5) { tableBodyCell(0) }
        nameInput(required: false, wait: 5) { tableBodyCell(0).$('input', name: 'name') }
        calories(required: false, wait: 5) { tableBodyCell(1) }
        caloriesInput(required: false, wait: 5) { tableBodyCell(1).$('input', name: 'calories') }
        createdAt(required: false, wait: 5) { tableBodyCell(2) }
        createdAtInput(required: false, wait: 5) { tableBodyCell(2).$('input', name: 'created_at') }
        editBtn(required: false, wait: 5) { tableBodyCell(3).$('data-ui': 'edit-btn') }
        deleteBtn(required: false, wait: 5) { tableBodyCell(4).$('data-ui': 'delete-btn') }
        cancelBtn(required: false, wait: 5) { tableBodyCell(3).$('data-ui': 'cancel-btn') }
        saveBtn(required: false, wait: 5) { tableBodyCell(4).$('data-ui': 'save-btn') }
    }
}
