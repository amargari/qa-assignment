package qa.assignment.modules

import geb.Module

class CommonElements extends Module {

    static content = {
        returnToHome { $('header', class: 'one column').$('h1') }
    }
}
