package qa.assignment.modules

import geb.Module

class SideMenu extends Module {

    static base = { $('aside', class:'two columns aside') }

    static content = {
        greeting { $('header', 'data-ui': 'greeting') }
        ediblesBtn { $('data-ui': 'edibles-btn').$('a') }
        profileBtn { $('data-ui': 'profile-btn').$('a') }
        logoutBtn { $('data-ui': 'logout-btn').$('span') }
    }
}
