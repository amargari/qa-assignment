package qa.assignment.modules

import geb.Module

class CaloriesTableHead extends Module {

    static content = {
        tableHeadCell { $('th', it) }
        name { tableHeadCell(0).text() }
        calories { tableHeadCell(1).text() }
        createdAt { tableHeadCell(2).text() }
        actions { tableHeadCell(3).text() }
    }
}
