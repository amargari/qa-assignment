package qa.assignment.helpers

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField

trait DateHelper {

    final Map ordinalNumbers = [1L: "1st", 2L: "2nd", 3L: "3rd", 21L: "21st", 22L: "22nd", 23L: "23rd", 31L: "31st",
                                4L: "4th", 5L: "5th", 6L: "6th", 7L: "7th", 8L: "8th", 9L: "9th", 10L: "10th",
                                11L: "11th", 12L: "12th", 13L: "13th", 14L: "14th", 15L: "15th", 16L: "16th",
                                17L: "17th", 18L: "18th", 19L: "19th", 20L: "20th", 24L: "24th", 25L: "25th",
                                26L: "26th", 27L: "27th", 28L: "28th", 29L: "29th", 30L: "30th"]

    final DateTimeFormatter READ_FORMAT = new DateTimeFormatterBuilder()
            .appendText(ChronoField.DAY_OF_MONTH, ordinalNumbers)
            .appendPattern(" MMM yyyy, HH:mm")
            .toFormatter()

    final DateTimeFormatter WRITE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")

    String generateDateTimeStamp(LocalDateTime inputDate) {
        inputDate.format(READ_FORMAT)
    }

    String daySuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    String capitilizeMonth(String inputString) {
        inputString = inputString.toLowerCase()
        inputString[0].toUpperCase() + inputString[1..2]
    }
}
