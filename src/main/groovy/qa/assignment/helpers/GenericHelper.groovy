package qa.assignment.helpers

trait GenericHelper {

    String letters = 'abcdefgijklmnopqrstuvwyz'

    String generateRandomName() {
        'Aleks' + generateRandomString(generateRandomInteger())
    }

    String convertUsernameToEmail(String username) {
        [username, 'test.com'].join('@').toLowerCase()
    }

    Integer generateRandomInteger() {
        Math.abs(new Random().nextInt() % (5 - 3)) + 5
    }

    String generateRandomString(int len) {
        StringBuilder stringBuilder = new StringBuilder()
        for (int i = 0; i < len; i++) {
            stringBuilder.append(letters.charAt(new Random().nextInt(letters.length())))
        }
        stringBuilder.toString()
    }
}
