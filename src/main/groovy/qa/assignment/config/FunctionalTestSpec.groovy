package qa.assignment.config

import geb.spock.GebReportingSpec
import qa.assignment.helpers.DateHelper
import qa.assignment.helpers.GenericHelper
import qa.assignment.pages.EdiblesPage
import qa.assignment.pages.HomePage
import qa.assignment.pages.RegistrationPage
import spock.lang.Shared

class FunctionalTestSpec extends GebReportingSpec implements GenericHelper, DateHelper {

    @Shared
    HomePage homePage

    @Shared
    String username

    @Shared
    static final String PASSWORD = 'test1234'

    EdiblesPage registerPlayer() {
        homePage = to HomePage
        homePage.register.click()
        username = generateRandomName()
        def registrationPage = fillRegistrationForm()
        registrationPage.submitForm.click()
        at EdiblesPage
    }

    RegistrationPage fillRegistrationForm() {
        RegistrationPage registrationPage = at RegistrationPage
        registrationPage.inputBox('name').value(username)
        registrationPage.inputBox('email').value(convertUsernameToEmail(username))
        registrationPage.inputBox('password').value(PASSWORD)
        registrationPage.inputBox('password-confirmation').value(PASSWORD)
        registrationPage
    }
}
