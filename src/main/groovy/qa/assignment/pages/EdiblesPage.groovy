package qa.assignment.pages

import geb.Page
import qa.assignment.modules.CaloriesTableBody
import qa.assignment.modules.CaloriesTableHead
import qa.assignment.modules.CommonElements
import qa.assignment.modules.SideMenu

class EdiblesPage extends Page {

    static url = 'edibles'

    static at = {
        waitFor { (browser.baseUrl + url) == driver.getCurrentUrl() &&
                caloriesInfoBox.displayed
        }
    }

    static content = {
        sideBar { module(SideMenu) }
        navigation { module(CommonElements) }

        tableTitle { $('header', 'data-ui': 'table-header') }
        // Waiting is required - When tested over slow connections the table is not loaded right away event though page is ready
        caloriesInfoBox(wait: 10) { $('div', 'data-ui': 'calory-box') }
        caloriesTable { $('table', 'data-ui': 'crud-table') }
        caloriesTableHead { caloriesTable.$('thead tr').module(CaloriesTableHead) }
        caloriesRecords { caloriesTable.$('tbody tr').moduleList(CaloriesTableBody) }
        caloriesTableSize(cache: false) { caloriesTable.$('tbody').$('tr').size() }

        addBtn { $('button', 'data-ui': 'add-btn') }

        filterBtn { $('button', 'data-ui': 'toggle-filter-btn') }
        filterStartDate { $(name: 'startDate', dynamic: true) }
        filterEndDate { $(name: 'endDate', dynamic: true) }
        filterStartHour { $(name: 'startHour', dynamic: true) }
        filterEndHour { $(name: 'endHour', dynamic: true) }
        filterSearchBtn { $('button', 'data-ui': 'filter-btn') }
    }

    Map<String, Object> caloriesCalc() {
        def caloriesData
        def matched = (caloriesInfoBox.text() =~ /([0-9]+)\/([0-9]+)/)
        if (matched.find()) {
            caloriesData = ['consumed': matched.group(1).toInteger(),
             'total': matched.group(2).toInteger()]
            calculateRemainingCalories(caloriesData)
        }
    }

    private Map<String, Object> calculateRemainingCalories(Map <String, Integer> calData) {
        if (calData['consumed'] <= calData['total']) {
            calData['remaining'] = calData['total'] - calData['consumed']
            calData['exceeded'] = false
        } else {
            calData['surpass'] = calData['consumed'] - calData['total']
            calData['exceeded'] = true
        }
        calData
    }
}
