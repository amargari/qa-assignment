package qa.assignment.pages

import geb.Page
import qa.assignment.modules.CommonElements

class RegistrationPage extends Page {

    static url = 'register'

    static at = {
        waitFor { (browser.baseUrl + url) == driver.getCurrentUrl() }
    }

    static content = {
        navigation { module(CommonElements) }
        form { $('#registration') }
        formTitle { form.$('h3') }
        inputLabel { option -> $('label', 'for': "$option-input").text() }
        inputBox { option-> $("#$option-input") }
        submitForm { form.$('input', 'type': 'submit') }
        footer { $('#registration').next('div') }
        loginButton { footer.$('a') }
        errorAlert(cache: false) { $('.flash') }
    }
}
