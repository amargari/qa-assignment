package qa.assignment.pages

import geb.Page
import qa.assignment.modules.CommonElements

class HomePage extends Page {

    static at = {
        waitFor { $('h1').displayed  }
    }

    static content = {
        navigation { module(CommonElements) }

        introPane { $('.u-full-width.centered') }
        homeButton { $('h1') }
        headerTitle { introPane.$('h2') }
        subtitle { introPane.$('p') }
        register { introPane.$('button').first() }
        login { introPane.$('button').last() }
    }
}
