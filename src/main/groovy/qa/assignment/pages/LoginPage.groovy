package qa.assignment.pages

import geb.Page
import qa.assignment.modules.CommonElements

class LoginPage extends Page {

    static url = 'login'

    static at = {
        waitFor { (browser.baseUrl + url) == driver.getCurrentUrl() }
    }
    static content = {
        navigation { module(CommonElements) }
        form { $('#login') }
        formTitle { form.$('h3').text() }
        // Available options are: email and password
        inputLabel { option -> $('label', 'for': "$option-input").text() }
        inputBox { option -> $("#$option-input") }
        loginButton { form.$('input', class: 'button') }
        footerNote { form.next('div') }
        registerNowButton { footerNote.$('a') }
        errorAlert(cache: false) { $('.flash') }
    }
}
