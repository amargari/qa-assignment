package qa.assignment.pages

import geb.Page
import qa.assignment.modules.CommonElements
import qa.assignment.modules.SideMenu

class ProfilePage extends Page {

    static url = 'profile'

    static at = {
        waitFor {
            (browser.baseUrl + url) == driver.getCurrentUrl() &&
                    formTitle.displayed
        }
    }

    static content = {
        sideBar { module(SideMenu) }
        navigation { module(CommonElements) }

        profileForm { $('form', 'data-ui': 'profile-form') }
        formTitle { profileForm.$('h3') }
        nameLabel { profileForm.$('label', 'for': 'name-input') }
        nameInputBox { profileForm.$('#name-input') }
        emailLabel { profileForm.$('label', 'for': 'email-input') }
        emailInputBox { profileForm.$('#email-input') }
        maxDailyCaloriesLabel { profileForm.$('label', 'for': 'max-daily-calories') }
        maxDailyCaloriesInput { profileForm.$('#max-daily-calories') }
        errorAlert(cache: false) { $('.flash') }
    }
}
