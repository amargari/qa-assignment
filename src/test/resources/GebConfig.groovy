import com.aoe.gebspockreports.GebReportingListener
import io.github.bonigarcia.wdm.WebDriverManager
import org.openqa.selenium.firefox.FirefoxDriver

// Configuration
reportingListener = new GebReportingListener()
reportsDir = 'build/geb-spock-reports'

// Driver configuration section
driver = {
    WebDriverManager.firefoxdriver().setup()
    new FirefoxDriver()
}

environments {
    firefox {
        atCheckWaiting = 1
        driver = { new FirefoxDriver() }
    }
}

baseUrl = "http://calories-calc.herokuapp.com/"