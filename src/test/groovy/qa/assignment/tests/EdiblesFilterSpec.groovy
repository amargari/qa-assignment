package qa.assignment.tests

import qa.assignment.config.FunctionalTestSpec
import qa.assignment.pages.EdiblesPage
import spock.lang.Shared

class EdiblesFilterSpec extends FunctionalTestSpec {

    @Shared
    EdiblesPage ediblesPage

    def setup() {
        ediblesPage = registerPlayer()
        waitFor { ediblesPage.caloriesInfoBox.classes().contains('green') }
    }

    def "Verify structure of the filter box"() {

        when: "user clicks on the filter button"
            ediblesPage.filterBtn.click()

        then: "start date, end date, start hour, end hour should become visible"
            waitFor { ediblesPage.filterStartDate.displayed }
            ediblesPage.filterEndDate.displayed
            ediblesPage.filterStartHour.displayed
            ediblesPage.filterEndHour.displayed

        and: "a button should exist for filter application"
            ediblesPage.filterSearchBtn.displayed
    }

    def "User filters on his edibles to filter out specific records"() {

        when: "user adds both of the meals, one for today and one for yesterday"
            addEdible(0, 'Burger', '2000')
            addEdible(1, 'Steak', '1500')
            addEdible(2, 'Salad', '450')

        and: "user edits steak record, moves it few days back"
            waitFor { ediblesPage.caloriesRecords[1].editBtn.displayed }
            ediblesPage.caloriesRecords[1].editBtn.click()
            ediblesPage.caloriesRecords[1].createdAtInput.value('2019-11-01T10:00')
            ediblesPage.caloriesRecords[1].saveBtn.click()

        and: "user edits salad record, moves it few days back"
            waitFor { ediblesPage.caloriesRecords[2].editBtn.displayed }
            ediblesPage.caloriesRecords[2].editBtn.click()
            ediblesPage.caloriesRecords[2].createdAtInput.value('2019-11-02T12:00')
            ediblesPage.caloriesRecords[2].saveBtn.click()

        then: "calories info table is get updated correctly"
            waitFor { ediblesPage.caloriesTableSize == 3 }
            ediblesPage.caloriesInfoBox.classes().contains('green')
            ediblesPage.caloriesCalc()['consumed'] == 2000

        when: "user selects to filter table records"
            ediblesPage.filterBtn.click()

        and: "user filter based on time"
            ediblesPage.filterStartHour << '01:00'
            ediblesPage.filterEndHour << '23:00'
            ediblesPage.filterSearchBtn.click()

        then: "records results are reduced to 1 record"
            waitFor {ediblesPage.caloriesTableSize == 1 }
            ediblesPage.caloriesRecords[0].name.text() == 'Burger'

        when: "user reset again the filters applied"
            ediblesPage.filterBtn.click()

        then: "all records are shown"
            waitFor {ediblesPage.caloriesTableSize == 3 }
    }

    private void addEdible(Integer caloriesRecord, String edibleName, String calories) {
        ediblesPage.addBtn.click()
        ediblesPage.caloriesRecords[caloriesRecord].nameInput.value(edibleName)
        ediblesPage.caloriesRecords[caloriesRecord].caloriesInput.value(calories)
        ediblesPage.caloriesRecords[caloriesRecord].saveBtn.click()
    }
}
