package qa.assignment.tests

import qa.assignment.config.FunctionalTestSpec
import qa.assignment.pages.EdiblesPage
import qa.assignment.pages.HomePage
import qa.assignment.pages.LoginPage
import qa.assignment.pages.RegistrationPage
import spock.lang.Shared
import spock.lang.Unroll

class RegistrationSpec extends FunctionalTestSpec {

    @Shared
    RegistrationPage registrationPage

    def setup() {
        homePage = to HomePage
        homePage.register.click()
        registrationPage = at RegistrationPage
    }

    def "Verify registration page structure"() {

        expect: "All titles have valid text"
            registrationPage.formTitle.text() == 'Create account'
            registrationPage.footer.text() == 'Already registered? Login'
            registrationPage.submitForm.value() == 'Join'

        and: "All form elements labels are correct"
            registrationPage.inputLabel('name') == 'Name'
            registrationPage.inputLabel('email') == 'Email'
            registrationPage.inputLabel('password') == 'Password'
            registrationPage.inputLabel('password-confirmation') == 'Password confirmation'

        and: "name and email input boxes are having correct placeholders"
            registrationPage.inputBox('name').attr('placeholder') == 'John Doe'
            registrationPage.inputBox('email').attr('placeholder') == 'john@example.com'

        and: "footer login button is redirecting user to home page"
            registrationPage.loginButton.attr('href') == browser.baseUrl + LoginPage.url
    }

    def "Verify that user can navigate from registration page to login page"() {

        when: "user is at registration page and selects to navigate to login page from the footer button"
            registrationPage.loginButton.click()

        then: "user is navigated to login page"
            at LoginPage
    }

    @Unroll
    def "Verify user will not try to register with empty fields - case #scenario"() {

        when: "user fills available input fields with the following data #scenario"
            registrationPage.inputBox('name').value(name)
            registrationPage.inputBox('email').value(email)
            registrationPage.inputBox('password').value(password)
            registrationPage.inputBox('password-confirmation').value(confPass)

        then: "user will remain in registration page"
            at RegistrationPage

        where:
            name        | email          | password  |  confPass | scenario
            ''          | 'test@test.com'| 'test1234'| 'test1234'| 'Name is not provided'
            'Alex Test' | ''             | 'test1234'| 'test1234'| 'Email is not provided'
            'Alex Test' | 'test@test.com'| ''        | 'test1234'| 'Password is not provided'
            'Alex Test' | 'test@test.com'| 'test1234'| ''        | 'Password confirmation is not provided'
            ''          | ''             | ''        | ''        | 'All input provided is empty'
    }

    def "Verify user registration will fail in case user enters mismatched input to password confirmation"() {
        given: "create user name for registration"
           String username = generateRandomName()

        when: "user completes the required fields to registration form"
            registrationPage.inputBox('name').value(username)
            registrationPage.inputBox('email').value(convertUsernameToEmail(username))
            registrationPage.inputBox('password').value(PASSWORD)
            registrationPage.inputBox('password-confirmation').value(PASSWORD + '1')

        and: "user clicks on register button"
            registrationPage.submitForm.click()

        then: "user is redirected to edibles page"
            at RegistrationPage
            waitFor { registrationPage.errorAlert.displayed }
            registrationPage.errorAlert.text() == "REGISTRATION FAILED. PLEASE TRY AGAIN"
    }

    def "Verify already registered user can't register again and error appears on registration page"() {

        given: "register user"
            String username = generateRandomName()
            fillInRegistrationForm(username)
            registrationPage.submitForm.click()

        and: "user is transferred to edibles page were he is logging out"
            EdiblesPage ediblesPage = at EdiblesPage
            ediblesPage.sideBar.logoutBtn.click()

        and: "user is logged out and transferred to home page where navigates to register page"
            homePage = at HomePage
            homePage.register.click()
            registrationPage = at RegistrationPage

        when: "user enters again the same data"
            fillInRegistrationForm(username)

        and: "presses to submit form"
            registrationPage.submitForm.click()

        then: "error appears informing user that registration failed"
            at RegistrationPage
            registrationPage.errorAlert.displayed
            registrationPage.errorAlert.text() == "REGISTRATION FAILED. PLEASE TRY AGAIN"
    }

    def "Verify user registration will fail in case user enters a password with less than 8 characters"() {

        given: "create user name for registration"
            String username = generateRandomName()

        when: "user completes the required fields to registration form"
            registrationPage.inputBox('name').value(username)
            registrationPage.inputBox('email').value(convertUsernameToEmail(username))
            registrationPage.inputBox('password').value('abc123')
            registrationPage.inputBox('password-confirmation').value('abc123')

        and: "user clicks on register button"
            registrationPage.submitForm.click()

        then: "user is redirected to edibles page"
            at RegistrationPage
            waitFor { registrationPage.errorAlert.displayed }
            registrationPage.errorAlert.text() == "REGISTRATION FAILED. PLEASE TRY AGAIN"
    }

    def "Verify user can register to calories calculator app"() {

        given: "create user name for registration"
            String username = generateRandomName()

        when: "user completes the required fields to registration form"
            registrationPage.inputBox('name').value(username)
            registrationPage.inputBox('email').value(convertUsernameToEmail(username))
            registrationPage.inputBox('password').value(PASSWORD)
            registrationPage.inputBox('password-confirmation').value(PASSWORD)

        and: "user clicks on register button"
            registrationPage.submitForm.click()

        then: "user is redirected to edibles page"
            at EdiblesPage
    }

    private void fillInRegistrationForm(String username) {
        registrationPage.inputBox('name').value(username)
        registrationPage.inputBox('email').value(convertUsernameToEmail(username))
        registrationPage.inputBox('password').value(PASSWORD)
        registrationPage.inputBox('password-confirmation').value(PASSWORD)
    }
}
