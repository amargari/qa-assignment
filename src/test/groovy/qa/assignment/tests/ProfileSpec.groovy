package qa.assignment.tests

import qa.assignment.config.FunctionalTestSpec
import org.openqa.selenium.Keys
import qa.assignment.pages.EdiblesPage
import qa.assignment.pages.HomePage
import qa.assignment.pages.LoginPage
import qa.assignment.pages.ProfilePage
import spock.lang.Unroll

class ProfileSpec extends FunctionalTestSpec {

    def setup() {
        EdiblesPage ediblesPage = registerPlayer()
        ediblesPage.sideBar.profileBtn.click()
    }

    def "verify profile page structure"() {

        expect: "user is redirected to profile page"
            ProfilePage profilePage = at ProfilePage

        and: "verify basic elements of the page"
            profilePage.formTitle.text() == "My profile"
            profilePage.nameLabel.text() == 'Name'
            profilePage.emailLabel.text() == 'Email'
            profilePage.maxDailyCaloriesLabel.text() == 'Max daily calories'

        and: "verify user details"
            profilePage.nameInputBox.value() == username
            profilePage.emailInputBox.value() == convertUsernameToEmail(username)
            profilePage.maxDailyCaloriesInput.value() == '3000'
    }

    @Unroll
    def "Verify user can't enter wrong value in calories"() {

        given: "get current calories value"
            ProfilePage profilePage = at ProfilePage
            def previousValue = profilePage.maxDailyCaloriesInput.value()

        when: "user tries to enter wrong value to calories input box"
            profilePage = at ProfilePage
            profilePage.maxDailyCaloriesInput.value(input)

        then: "input field is left empty"
            profilePage.maxDailyCaloriesInput.value() == ""

        cleanup: "restore calories field"
            profilePage.maxDailyCaloriesInput.value(previousValue)

        where:
            input << ['abcd', '@#$', '_-=']
    }

    def "Verify that when user modifies his name the greeting is also gets updated"() {

        given: "user is on profile page from previous step"
            ProfilePage profilePage = at ProfilePage

        and: "greeting shows the old username"
            profilePage.sideBar.greeting.text() == "Hi $username"

        when: "user updates his name to Alexandros"
            profilePage.nameInputBox.value('Alexandros')
            profilePage.nameInputBox << Keys.TAB

        then: "the greeting is also updated"
            waitFor { profilePage.sideBar.greeting.text() == 'Hi Alexandros' }

        cleanup: "return username to what it was before"
            profilePage.nameInputBox.value(username)
            profilePage.nameInputBox << Keys.TAB
    }

    def "verify user who updated his email address is able to login with the new mail"() {

        given: "user remains at profile page"
            ProfilePage profilePage = at ProfilePage

        when: "user updates his email address"
            profilePage.emailInputBox.value('mynewemail@email.com')
            profilePage.emailInputBox << Keys.TAB

        and: "user logs out"
            profilePage.navigation.returnToHome.click()

        then: "user is navigated to home page and is logged out"
            at HomePage

        when: "user logs in and uses his new email address this time"
            LoginPage loginPage = to LoginPage
            waitFor { loginPage.loginButton.displayed }
            loginPage.inputBox('email').value('mynewemail@email.com')
            loginPage.inputBox('password').value(PASSWORD)
            loginPage.loginButton.click()

        then: "user is redirected to edibles page"
            EdiblesPage ediblesPage = at EdiblesPage

        cleanup: "reset email address to initial one"
            ediblesPage.sideBar.profileBtn.click()
            profilePage = at ProfilePage
            profilePage.emailInputBox.value(convertUsernameToEmail(username))
            profilePage.emailInputBox << Keys.TAB
    }

    def "Verify user updates his max daily calories and it is updated to edibles page"() {

        when: "user is at profile page and navigates via the side bar to edibles page"
            ProfilePage profilePage = at ProfilePage
            profilePage.sideBar.ediblesBtn.click()
            EdiblesPage ediblesPage = at EdiblesPage

        then: "user checks for the daily limit set of calories"
            waitFor { ediblesPage.caloriesCalc()['total'] == 3000 }

        when: "user returns to profile page and sets the daily calory limit to 5000"
            ediblesPage.sideBar.profileBtn.click()
            profilePage = at ProfilePage
            profilePage.maxDailyCaloriesInput.value(5000)

        and: "user navigates to edibles page again"
            profilePage.sideBar.ediblesBtn.click()
            ediblesPage = at EdiblesPage

        then: "user is visiting again edibles page and sees that the limit has changes to 5000"
            waitFor { ediblesPage.caloriesCalc()['total'] == 5000 }
    }

    def "Verify that empty name field will raise an error"() {

        when: "user is at Profile page"
            ProfilePage profilePage = at ProfilePage
            profilePage.nameInputBox = ''
            profilePage.nameInputBox << Keys.TAB

        then: "error appears on the top of the page"
            waitFor { profilePage.errorAlert.displayed }
            profilePage.errorAlert.text() == 'UNPROCESSABLE ENTITY'
    }
}
