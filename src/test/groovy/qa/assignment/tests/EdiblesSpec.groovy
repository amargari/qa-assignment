package qa.assignment.tests

import qa.assignment.config.FunctionalTestSpec
import qa.assignment.pages.EdiblesPage
import spock.lang.Shared

import java.time.LocalDateTime

class EdiblesSpec extends FunctionalTestSpec {

    @Shared
    EdiblesPage ediblesPage

    def setup() {
        ediblesPage = registerPlayer()
    }

    def 'Verify edibles page structure'() {

        expect: 'user is landed on edibles page and a greeting message appears over sidebar'
            waitFor { ediblesPage.sideBar.greeting.text() == "Hi $username" }

        and: 'page title and today calories consumption is shown'
            ediblesPage.tableTitle.text() == 'Edibles'
            ediblesPage.caloriesInfoBox.displayed
            ediblesPage.caloriesInfoBox.text() == 'TODAY 0/3000 CALS'
            ediblesPage.caloriesInfoBox.classes().contains('green')

        and: 'user is shown with an empty table'
            ediblesPage.caloriesTableHead.name == 'Name'
            ediblesPage.caloriesTableHead.calories == 'Calories'
            ediblesPage.caloriesTableHead.createdAt == 'Created at'
            ediblesPage.caloriesTableHead.actions == 'Actions'

        and: 'table records should be empty - a place holder massage informing user that nothing exist should appear'
            ediblesPage.caloriesTableSize == 1
            ediblesPage.caloriesRecords[0].emptyMessage.text() == 'Nothing found :('
    }

    def 'User tries to add a menu but then deletes it before it is saved'() {

        when: 'user clicks on the add button'
            waitFor { ediblesPage.addBtn.displayed }
            ediblesPage.addBtn.click()

        then: 'new entry line appears in the table with placeholders as guidance'
            ediblesPage.caloriesTableSize == 1
            ediblesPage.caloriesRecords[0].nameInput.attr('placeholder') == 'e.g. Pizza'
            ediblesPage.caloriesRecords[0].caloriesInput.attr('placeholder') == '200'
            ediblesPage.caloriesRecords[0].createdAtInput.value() == LocalDateTime.now().format(WRITE_FORMAT)
            ediblesPage.caloriesRecords[0].cancelBtn.displayed
            ediblesPage.caloriesRecords[0].saveBtn.displayed

        when: 'user fills in one menu - burger time'
            ediblesPage.caloriesRecords[0].nameInput.value('Burger')
            ediblesPage.caloriesRecords[0].caloriesInput.value('1200')
            ediblesPage.caloriesRecords[0].createdAtInput.value(LocalDateTime.now().minusHours(2).format(WRITE_FORMAT))

        and: 'presses the cancel button'
            ediblesPage.caloriesRecords[0].cancelBtn.click()

        then: 'record is deleted'
            ediblesPage.caloriesTableSize == 1
            ediblesPage.caloriesRecords[0].emptyMessage.text() == 'Nothing found :('
    }

    def 'User tries to enter some edible and saves created records'() {

        given: 'some initialization'
            def burgerTime = LocalDateTime.now().minusHours(2)
            def pastaTime = LocalDateTime.now().minusHours(1).minusMinutes(10)

        when: 'user clicks on add button'
            waitFor { ediblesPage.addBtn.displayed }
            ediblesPage.addBtn.click()

        and: 'then adds a burger edible'
            ediblesPage.caloriesRecords[0].nameInput.value('Burger')
            ediblesPage.caloriesRecords[0].caloriesInput.value('1200')
            ediblesPage.caloriesRecords[0].createdAtInput.value(burgerTime.format(WRITE_FORMAT))

        and: 'saves it'
            ediblesPage.caloriesRecords[0].saveBtn.click()

        then: 'record is stores and table is shown the newly created record'
            ediblesPage.caloriesTableSize == 1
            assertTableEntry(ediblesPage.caloriesRecords[0], 'Burger', '1200', burgerTime.format(READ_FORMAT))

        when: 'user adds another menu'
            ediblesPage.addBtn.click()
            addEdible(ediblesPage.caloriesRecords[1], 'Pasta', '750', pastaTime.format(WRITE_FORMAT))
            ediblesPage.caloriesRecords[1].saveBtn.click()

        then: "evaluate second record and check the remaining calories"
            waitFor { ediblesPage.caloriesTableSize == 2 }
            assertTableEntry(ediblesPage.caloriesRecords[1], 'Pasta', '750', pastaTime.format(READ_FORMAT))

        and: "verify correct calculation of remaining calories"
            def caloriesInfo = ediblesPage.caloriesCalc()
            caloriesInfo['consumed'] == 1950
            caloriesInfo['exceeded'] == false

        and: "since calories consumed today don't exceed the limit then information box is highlighted as green"
            ediblesPage.caloriesInfoBox.classes().contains('green')
    }

    def 'User adds an edible and then modifies it'() {

        given: 'some initialization'
            def burgerTime = LocalDateTime.now().minusHours(2)

        when: 'user clicks on add button and enters an edible'
            waitFor { ediblesPage.addBtn.displayed }
            ediblesPage.addBtn.click()
            addEdible(ediblesPage.caloriesRecords[0], 'Burger', '1000', burgerTime.format(WRITE_FORMAT))

        and: "user clicks on save it"
            ediblesPage.caloriesRecords[0].saveBtn.click()

        then: "record is created correctly"
            waitFor { ediblesPage.caloriesRecords[0].editBtn.displayed }
            ediblesPage.caloriesRecords[0].deleteBtn.displayed
            assertTableEntry(ediblesPage.caloriesRecords[0], 'Burger', '1000', burgerTime.format(READ_FORMAT))

        when: "user then wants to change calories for this lunch and the date it occurred"
            ediblesPage.caloriesRecords[0].editBtn.click()

        then: "record is open for editing"
            waitFor { ediblesPage.caloriesRecords[0].cancelBtn.displayed }
            ediblesPage.caloriesRecords[0].saveBtn.displayed

        when: "user enter new values for time and calories"
            ediblesPage.caloriesRecords[0].caloriesInput.value('1200')
            ediblesPage.caloriesRecords[0].createdAtInput.value(burgerTime.minusHours(1).format(WRITE_FORMAT))
            ediblesPage.caloriesRecords[0].saveBtn.click()

        then:
            waitFor { ediblesPage.caloriesRecords[0].calories.text() == '1200' }
            ediblesPage.caloriesRecords[0].createdAt.text() == burgerTime.minusHours(1).format(READ_FORMAT)
    }

    def 'User adds edibles that exceed daily calories consumption'() {

        expect:
            ediblesPage.caloriesInfoBox.classes().contains('green')

        when: 'some initialization'
            def eggTime = LocalDateTime.now().minusHours(4)
            def burgerTime = LocalDateTime.now().minusHours(4)
            def steakTime = LocalDateTime.now().minusHours(1)

        and: "user add the first two meals of the day"
            ediblesPage.addBtn.click()
            addEdible(ediblesPage.caloriesRecords[0], 'Eggs', '300', eggTime.format(WRITE_FORMAT))
            ediblesPage.caloriesRecords[0].saveBtn.click()
            ediblesPage.addBtn.click()
            addEdible(ediblesPage.caloriesRecords[1], 'Burger', '1500', burgerTime.format(WRITE_FORMAT))
            ediblesPage.caloriesRecords[1].saveBtn.click()

        then: "verify that calories box is updated accordingly"
            waitFor {ediblesPage.caloriesTableSize == 2 }
            ediblesPage.caloriesInfoBox.classes().contains('green')
            def caloriesInfo = ediblesPage.caloriesCalc()
            caloriesInfo['consumed'] == 1800

        when: "user adds also his dinner"
            ediblesPage.addBtn.click()
            addEdible(ediblesPage.caloriesRecords[2], 'Steak with chips', '1300', steakTime.format(WRITE_FORMAT))
            ediblesPage.caloriesRecords[2].saveBtn.click()

        then: "verify that user exceeded his daily limit of calories consumption"
            waitFor {ediblesPage.caloriesTableSize == 3 }
            ediblesPage.caloriesInfoBox.classes().contains('red')
            ediblesPage.caloriesCalc()['consumed'] == 3100
    }

    def 'User adds an edible which exceeds daily calories consumption, then modifies it\'s date and daily consumption is less that the limit'() {

        expect:
            ediblesPage.caloriesInfoBox.classes().contains('green')

        when: 'some initialization'
            def burgerTime = LocalDateTime.now().minusHours(4)
            def steakTime = LocalDateTime.now().minusHours(1)

        and: "user add the first two meals of the day"
            ediblesPage.addBtn.click()
            addEdible(ediblesPage.caloriesRecords[0], 'Burger', '2000', burgerTime.format(WRITE_FORMAT))
            ediblesPage.caloriesRecords[0].saveBtn.click()
            ediblesPage.addBtn.click()
            addEdible(ediblesPage.caloriesRecords[1], 'Steak', '1500', steakTime.format(WRITE_FORMAT))
            ediblesPage.caloriesRecords[1].saveBtn.click()

        then: "calories information box should shown that user has exceeded his daily calories limit"
            waitFor { ediblesPage.caloriesTableSize == 2 }
            ediblesPage.caloriesInfoBox.classes().contains('red')

        when: "user then edits second lunch and changes its date by one day less"
            ediblesPage.caloriesRecords[1].editBtn.click()
            ediblesPage.caloriesRecords[1].createdAtInput.value(steakTime.minusDays(1).format(WRITE_FORMAT))
            ediblesPage.caloriesRecords[1].saveBtn.click()

        then: "calories info box is updated and message highlighting changes to green"
            waitFor { ediblesPage.caloriesInfoBox.classes().contains('green') }
            ediblesPage.caloriesCalc()['consumed'] == 2000
    }

    private void addEdible(def record, String edibleName, String calories, String date) {
        record.nameInput.value(edibleName)
        record.caloriesInput.value(calories)
        record.createdAtInput.value(date)
    }

    private void assertTableEntry(def record, String name, String calories, String date) {
        assert record.name.text() == name
        assert record.calories.text() == calories
        assert record.createdAt.text() == date
    }
}
