package qa.assignment.tests

import qa.assignment.config.FunctionalTestSpec
import qa.assignment.pages.HomePage
import qa.assignment.pages.LoginPage
import qa.assignment.pages.RegistrationPage
import spock.lang.Unroll

class HomePageSpec extends FunctionalTestSpec {

    def "User tries to navigate to app"() {
        when: "navigated to calories-calc.herokuapp.com"
            homePage = to HomePage

        then: "home page should appear"
            at HomePage

        and: "would show all the appropriate elements"
            homePage.headerTitle.displayed
            homePage.subtitle.displayed

        and: "elements would have the appropriate text"
            homePage.headerTitle.text() == "Welcome to Calc"
            homePage.subtitle.text() == "Input your calories"

        and: "two buttons exist with correct text"
            homePage.register.text() == "REGISTER"
            homePage.login.text() == "LOGIN"
    }

    @Unroll
    def "Verify user ability to navigate from #targetPage page and then back to home"() {

        when: "user navigates to home page"
            homePage = to HomePage

        and: "selects to #targetPage"
            homePage."$targetPage".click()

        then: "user will be navigated to #resultPage"
            def landedPage = at resultPage

        when: "user uses the upper left header to return to home"
            landedPage.navigation.returnToHome.click()

        then: "user is landed on home page"
            at HomePage

        where:
            targetPage | resultPage
            'register' | RegistrationPage
            'login'    | LoginPage
    }

}
