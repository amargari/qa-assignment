package qa.assignment.tests

import qa.assignment.config.FunctionalTestSpec
import qa.assignment.pages.EdiblesPage
import qa.assignment.pages.HomePage
import qa.assignment.pages.LoginPage
import qa.assignment.pages.RegistrationPage
import spock.lang.Shared

class LoginSpec extends FunctionalTestSpec {

    @Shared
    LoginPage loginPage

    static final String EMAIL = 'alex.test@test.com'

    def setup() {
        homePage = to HomePage
        homePage.login.click()
        loginPage = at LoginPage
    }

    def "Verify login page basic structure"() {

        expect: "All titles to be visible and have the correct text"
            loginPage.formTitle == 'Login'

        and: "input options provided to user are having correct labels"
            loginPage.inputLabel('email') == 'Email'
            loginPage.inputLabel('password') == 'Password'

        and: "have valid placeholders"
            loginPage.inputBox('email').attr('placeholder') == 'john@example.com'

        and: "after the form should exist the option to allow user to register"
            loginPage.footerNote.text() == "Haven't registered? Register now"
            loginPage.registerNowButton.attr('href') == browser.baseUrl + RegistrationPage.url
    }

    def "Verify user can navigate to register page from login page"() {

        when: "user navigated to login page and clicks on register now"
            loginPage.registerNowButton.click()

        then: "user is landed to registration page"
            at RegistrationPage
    }

    def "Verify that user can login to calories calculator application"() {

        when: "user enters valid email and password"
            loginPage.inputBox('email').value(EMAIL)
            loginPage.inputBox('password').value(PASSWORD)

        and: "clicks submit"
            loginPage.loginButton.click()

        then: "user is logged in and transferred to home page of the application which is the edibles page"
            EdiblesPage ediblesPage = at EdiblesPage

        when: "user wants to logout"
            waitFor { ediblesPage.sideBar.logoutBtn.displayed }
            ediblesPage.sideBar.logoutBtn.click()

        then: "user logged out and sent to home page"
            at HomePage
    }

    def "Verify user with invalid credentials will not be able to login to application"() {

        when: "user with invalid credentials tries to login - user enters his details"
            loginPage.inputBox('email').value('thisisaninvalidemail@email.com')
            loginPage.inputBox('password').value('invalidPassword')

        and: "user clicks to login"
            loginPage.loginButton.click()

        then: "user remains on same page"
            at LoginPage

        and: "Error appears informing him that login attempt was unsuccessful"
            waitFor { loginPage.errorAlert.displayed }
            loginPage.errorAlert.text() == "LOGIN FAILED. PLEASE TRY AGAIN"
    }

    def "Verify valid user will be able to login logout and then login again to app"() {

        when: "user enters valid email and password"
            loginPage.inputBox('email').value(EMAIL)
            loginPage.inputBox('password').value(PASSWORD)

        and: "clicks submit"
            loginPage.loginButton.click()

        then: "user is logged in and transferred to home page of the application which is the edibles page"
            EdiblesPage ediblesPage = at EdiblesPage

        when: "user logs out by the top left corner Calc button"
            ediblesPage.navigation.returnToHome.click()

        then: "user is logged out and transferred to home"
            at HomePage

        when: ""
            homePage = at HomePage
            homePage.login.click()
            loginPage = at LoginPage

        and: "enters his valid credentials and clicks submit"
            loginPage.inputBox('email').value(EMAIL)
            loginPage.inputBox('password').value(PASSWORD)
            loginPage.loginButton.click()

        then: "user is transferred to edibles page"
            at EdiblesPage
    }
}
